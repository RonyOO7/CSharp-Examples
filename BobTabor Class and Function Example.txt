using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BobTabor
{
    class Program
    {
        static void Main(string[] args)
        {
            Car myCar = new Car();

            Car.MyMethod();
            //myCar.Make = "Automobile";
            //myCar.Model = "Prius";
            //myCar.Year = 2018;
            //myCar.Color = "blue";

            //Console.WriteLine("{0} {1} {2} {3}", myCar.Make, myCar.Model,myCar.Year,myCar.Color);

            //Car mySecondCar = new Car("Ford", "Mustang", 2011, "blue");
            //Console.WriteLine("{0} {1} {2} {3}", mySecondCar.Make, myCar.Model,myCar.Year,myCar.Color);

            //decimal val = Determine(myCar);
            //Console.WriteLine("{0:C}",val);
            Console.ReadLine();
        }

        //private static decimal Determine(Car car)
        //{
        //    decimal value = 200.0M;

        //    return value;
        //}
    }

    class Car
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string Color { get; set; }

        //public Car()
        //{
        //    Make = "Nissan";
        //}

        //Overloaded Constructor
        //public Car(string make,string model,int year,string color)
        //{
        //    Make = make;
        //    Model = model;
        //    Year = year;
        //    Color = color;
        //}

        public static void MyMethod()
        {
            Console.WriteLine("Called Static Method");
        }
    }
}
